mainRoutes.$inject = ['$stateProvider'];

export function mainRoutes($stateProvider: any) {

    $stateProvider.state('overview', {
        url: '',
        component: 'loginComponent'
    });

    $stateProvider.state('patients', {
        url: '/patients',
        component: 'patientsContainerComponent'
    });

    $stateProvider.state('admin', {
        url: '/admin',
        template: '<div class="wrapper"><h1> Admin </h1></div>'
    });
}

import { Component, OnInit } from 'angular-ts-decorators';
import './menu.component.scss';
import { MENUSTATE, MENUITEMSDATA } from './menu.constant';


const templateUrl = require('./menu.component.html');


@Component({
  selector: 'menu-component',
  template: templateUrl
})
export class MenuComponent implements OnInit {
  static $inject = ['$mdSidenav'];
  menuState: string;
  menuItemsData: any;

  constructor(
    private $mdSidenav: any,
    
  ) { };


  ngOnInit() {
    this.menuState = MENUSTATE.closed;
    this.menuItemsData = MENUITEMSDATA;
  }

  toggleLeft() {
    this.buildToggler('sidebar');
    this.changeMenuState(this.menuState);
  }
  
  buildToggler(componentId) {
    this.$mdSidenav(componentId).toggle();
  }

  changeMenuState(state) {
    this.menuState = state === MENUSTATE.closed ? MENUSTATE.opened : MENUSTATE.closed;
  }
}

import { MenuItemsInterface, MenuStateInterface } from './menu.interface';

export const MENUSTATE: MenuStateInterface = {

            opened: 'keyboard_arrow_left',
            closed: 'keyboard_arrow_right'
         
    };

export const MENUITEMSDATA: MenuItemsInterface = [
        
    {
        name: 'Overview',
        icon: 'poll',
        url: 'overview'
    },
    {
        name: 'Patients',
        icon: 'people',
        url: 'patients'
    },
    {
        name: 'Admin',
        icon: 'account_circle',
        url: 'admin'
    }         
            
];

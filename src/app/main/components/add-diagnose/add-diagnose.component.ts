import './add-diagnose.component.scss';
import * as angular from 'angular';
import * as _ from 'lodash'; 
import { Component, Input, Output } from 'angular-ts-decorators';
import { NewDiagnose } from './add-diagnose.interface'
import * as PatientActions from '../../../actions/patient.actions';

const templateUrl = require('./add-diagnose.component.html');

@Component({
    selector: 'add-diagnose-component',
    template: templateUrl
})

export class AddDiagnoseComponent {
    @Input() patient;

  static $inject = ['$ngRedux'];
    formDiagnose: NewDiagnose;
    diagnose: NewDiagnose;
    formState = false;
    addDiagnoseState = true;
    
    constructor(
      private $ngRedux
     ) { };
    
    showDiagnoseForm() {
      this.formState = !this.formState;
      this.addDiagnoseState = !this.addDiagnoseState;
      !this.formState && this.resetForm(this.formDiagnose);
    }
  
    resetForm(formData) {
      angular.copy({}, formData);
    }

    addDiagnose(isValid) {

      if(isValid) {
        const idDiagnose = _.isEmpty(this.patient.diagnoses) ? 0 : _.head(this.patient.diagnoses)['id'] + 1; 
        this.diagnose = {
          id: idDiagnose,
          ...this.formDiagnose,
          date: new Date(),
          resolved: ''
        };
        this.showDiagnoseForm();

        let patientCurrent = {...this.patient};
        patientCurrent.diagnoses = patientCurrent.diagnoses.concat(this.diagnose).sort((a, b) => b.id - a.id);
        this.$ngRedux.dispatch(PatientActions.addPatientDiagnoseToState({...patientCurrent}));
      }
    }
}


import './login.component.scss';
import * as angular from 'angular';
import { Component, OnChanges } from 'angular-ts-decorators';
import { LoginService } from './login.service';
import * as LogActions from '../../../actions/log.actions';
import * as UserActions from '../../../actions/user.actions';

@Component({
  selector: 'login-component',
  template: require('./login.component.html')
})

export class LoginComponent implements OnChanges {

  static $inject = ['$mdDialog', 'LoginService', '$ngRedux'];

  errorMessage: string;
  log;
  user;
  username: string;
  password: string;

  constructor(private $mdDialog: any, private LoginService: LoginService, private $ngRedux) { };

  ngOnChanges() {
    this.$ngRedux.subscribe(() => {
      let state = this.$ngRedux.getState();
      this.user = state.user;
  });
}

  showLogin() {
    this.$mdDialog.show({
      contentElement: '#loginDialog',
      parent: angular.element(document.body),
      clickOutsideToClose: true
    });
    this.errorMessage = '';
  };

  login() {
    this.LoginService.login(this.username, this.password)
      .then(response => {
        if (response.data.success) {
          window.localStorage.setItem('LOCAL_TOKEN_KEY', response.data.token);
          this.log = true;
          this.$ngRedux.dispatch(LogActions.addLog(this.log));
          this.$mdDialog.hide();
          this.user = this.username;
          this.$ngRedux.dispatch(UserActions.addUser(this.user));
          this.username = '';
          this.password = '';
        } else {
          this.errorMessage = 'Invalid username/password! Please, check the values';
        }
      });
  }

  toggleLogin(state) {
    this.$mdDialog.hide();
  }
};


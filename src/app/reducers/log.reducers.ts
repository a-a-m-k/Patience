import { LOG } from '../constants/log';

const initialState = [];

export function LogReducer(state = initialState, action) {
  switch (action.type) {
    case LOG.ADD_LOG:
      return [...state, action.payload];
    case LOG.SET_LOG:
      return action.payload;
    default:
      return state;
  }
}

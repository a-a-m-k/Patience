import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { TableActiveComponent } from './table-active.component';
import { TableActiveService } from './table-active.service';
import { TableFormModule } from './table-form/table-form.module';
import ngMaterial = require('angular-material');
import uiGrid = require('angular-ui-grid');
import 'angular-material/angular-material.css';
import 'angular-ui-grid/ui-grid.css';


@NgModule({
  imports: [
    TableFormModule,
    ngMaterial,
    uiGrid,
    'ui.grid',
    'ui.grid.edit',
    'ui.grid.cellNav',
    'ui.grid.expandable'
    
  ],
  declarations: [TableActiveComponent],
  providers: [
    { provide: 'TableActiveService', useClass: TableActiveService }
  ]
})

export class TableActiveModule {
}


import * as _ from 'lodash';
import './widgets-wrapper.component.scss';
import { Component, Input, OnInit } from 'angular-ts-decorators';
import { WIDGETS_DATA } from './widgets-wrapper.constant';

const templateUrl = require('./widgets-wrapper.component.html');   

@Component({
  selector: 'widgetsWrapperComponent',
  template: templateUrl
})

export class WidgetsWrapperComponent implements OnInit {
  @Input() selectedPatient;
  properties;
  selected: string[];
  
  ngOnInit () {
    this.properties = WIDGETS_DATA;
    this.selected = [WIDGETS_DATA[0].widget, WIDGETS_DATA[1].widget];
  }
    
  activeWidget(widget) {
    return _.includes(this.selected, widget);
  }

  selectedWidgets(selected) {
    this.selected = selected;
  }
      
} 


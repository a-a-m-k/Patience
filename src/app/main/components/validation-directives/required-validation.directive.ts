import * as angular from 'angular';
import { IScope, IAugmentedJQuery, IAttributes, INgModelController } from 'angular';
import { Directive } from 'angular-ts-decorators';

@Directive({
  selector: 'required-validation-directive',
  require: 'ngModel',
 restrict:'A',
 link:function(scope: IScope, element: IAugmentedJQuery, attrs: IAttributes, ngModel: INgModelController) {
    ngModel.$parsers.push(function(value) {
        ngModel.$setValidity('empty', /.*\S.*/.test(value)); 
      return value;
      });
    
  }

})
export class RequiredValidationDirective {}
import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class  DiagnosesTablesEditService {
  static $inject = ['$http'];
    
  constructor(private $http: ng.IHttpService) {}
  
  public getEditedDiagnose() {
    return {
      name:'',
      date: '',
      diagnose: '',
      resolved: ''     
    };
  }
}


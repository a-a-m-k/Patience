import { NgModule } from 'angular-ts-decorators';
import * as angular from 'angular';
import { AddPatientModule } from './add-patient/add-patient.module';
import { diagnosesModule } from './diagnoses/diagnoses.module';
import { AddMedicationModule } from './add-medication/add-medication.module';
import { MedicationsModule } from './medications/medications.module';
import { tabsModule } from './nav-tabs/tabs.module';
import { PatientInfoModule } from './patient-info/patient-info.module';
import { ShowPatientsListModule } from './show-patients-list/show-patients-list.module';
import { searchModule } from './patient-search/patient-search.module';
import { headerModule } from './header/header.module';
import { PatientEditModule} from './patient-edit/patient-edit.module';

export const componentsModule = angular
  .module('app.main.component', [
    PatientEditModule.name,
    AddPatientModule.name,
    tabsModule,
    diagnosesModule,
    MedicationsModule.name,
    AddMedicationModule.name,
    PatientInfoModule.name,      
    ShowPatientsListModule.name,    
    searchModule.name,
    headerModule
  ])
  .name;
  
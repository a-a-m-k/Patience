import { PATIENTS } from '../constants/patients';

export function addPatientToState(patient) {
  return {
    type: PATIENTS.ADD_PATIENT,
    payload: patient
  };
}
export function editPatient(patient) {
  return {
    type: PATIENTS.EDIT_PATIENT,
    payload: patient
  }
}


export function setPatientsToState(patients) {
  return {
    type: PATIENTS.SET_PATIENTS,
    payload: patients
  };
}

export function addPatientDiagnoseToState(patient) {
  return {
    type: PATIENTS.ADD_PATIENT_DIAGNOSE,
    payload: patient
  };
} 

export function removeActiveMedication(patient) {
  return {
    type: PATIENTS.REMOVE_ACTIVE_MEDICATION,
    payload: patient
  }
}  

export function updateMedication(patient) {
  return {
    type: PATIENTS.UPDATE_MEDICATION,
    payload: patient
  }
}

export function addPatientMedication(patient) {
  return {
    type: PATIENTS.ADD_PATIENT_MEDICATION,
    payload: patient
  }
}

export function updateDiagnoses(patient) {
  return {
    type: PATIENTS.UPDATE_DIAGNOSES,
    payload: patient
  };
}

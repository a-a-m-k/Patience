import { USER } from '../constants/user';

const initialState = [];

export function UserReducer(state = initialState, action) {
  switch (action.type) {
    case USER.ADD_USER:
      return [...state, action.payload];
    case USER.SET_USER:
      return action.payload;
    default:
      return state;
  }
}

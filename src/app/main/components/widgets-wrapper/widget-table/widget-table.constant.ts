import { NamesBtn } from './widget-table.interfaces';

export const CONSTANTS: NamesBtn = {
    previous: 'previous',
    latest: 'latest'
};

import * as angular from 'angular';
import { IScope, IAugmentedJQuery, IAttributes, INgModelController } from 'angular';
import { Directive } from 'angular-ts-decorators';

@Directive({
  selector: 'phone-validation-directive',
  require: 'ngModel',
 restrict:'A',
 link:function (scope: IScope, element: IAugmentedJQuery, attrs: IAttributes, ngModel: INgModelController) {
    ngModel.$parsers.push (function(value) {
        ngModel.$setValidity ('phone', /^((8|\+38)-?)?(\(?0[0-9][0-9]\)?)?-?\d{3}-?\d{2}-?\d{2}$/.test(value));
        return value;
      });
    
  }

})
export class PhoneValidationDirective {}
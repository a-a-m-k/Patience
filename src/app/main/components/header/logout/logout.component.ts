import './logout.component.scss';
import { Component, OnInit } from 'angular-ts-decorators';
import * as LogActions from '../../../../actions/log.actions';
import * as UserActions from '../../../../actions/user.actions';

@Component({
  selector: 'logout-component',
  template: require('./logout.component.html')
})

export class LogoutComponent implements OnInit {

  static $inject = ['$ngRedux'];

  log;
  user;

  constructor( private $ngRedux ) {}

  ngOnInit() {
    this.$ngRedux.subscribe(() => {
    let state = this.$ngRedux.getState();
    this.log = state.log;
    this.user = state.user.toString();
  });
}

  logout() {
    window.localStorage.removeItem('LOCAL_TOKEN_KEY');
    this.log = false;
    this.$ngRedux.dispatch(LogActions.setLogToState(this.log));
    this.user = '';
    this.$ngRedux.dispatch(UserActions.setUserToState(this.user));
  }

}


import './show-patients-list.component.scss';
import { Component, Input, Output, OnChanges } from 'angular-ts-decorators';
import { ShowPatientsListService } from './show-patients-list.service';

const templateUrl = require('./show-patients-list.component.html');

@Component({
  selector: 'show-patients-list-component',
  template: templateUrl
})

export class ShowPatientsListComponent implements OnChanges {
  @Input() patients;
  @Output() onSelectPatient;

  static $inject = ['ShowPatientsListService'];
  sortedPatients;

  constructor(
    private showPatientsListService: ShowPatientsListService
  ) { };

  ngOnChanges(changes) {
    if (changes.patients.currentValue) {
      this.sortedPatients = this.showPatientsListService.sortPatientsList(this.patients);
    }
  }

  selectPatient(index) {
    this.onSelectPatient({ index });
  }
}

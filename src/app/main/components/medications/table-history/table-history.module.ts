import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { TableHistoryComponent } from './table-history.component';
import { TableHistoryService } from './table-history.service';
import { TableHistoryFormModule } from './table-history-form/table-history-form.module';
import ngMaterial = require('angular-material');
import uiGrid = require('angular-ui-grid');
import 'angular-material/angular-material.css';
import 'angular-ui-grid/ui-grid.css';


@NgModule({
  imports: [
    TableHistoryFormModule,
    ngMaterial,
    uiGrid,
    'ui.grid',
    'ui.grid.edit',
    'ui.grid.cellNav'
    
  ],
  declarations: [TableHistoryComponent],
  providers: [
    { provide: 'TableHistoryService', useClass: TableHistoryService }
  ]
})

export class TableHistoryModule {
}


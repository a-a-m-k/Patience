import './widgets-settings.component.scss';
import * as angular from 'angular';
import * as _ from 'lodash';
import { Component, Output } from 'angular-ts-decorators';
import { settingConst } from './widgets-settings.constant'

const templateUrl = require('./widgets-settings.component.html');

@Component({
    selector: 'widgets-settings-component',
    template: templateUrl
})

export class widgetsSettingsComponent {
    @Output() onSelected;

    static $inject = ['$mdDialog'];
    items = [...settingConst];
    selected = [...settingConst];
    widgetsStatus = false;

    constructor(
        private $mdDialog: any
    ) { };

    showTabDialog() {
        this.$mdDialog.show({
            contentElement: '#widgetsDialog',
            parent: angular.element(document.body)
        });
    }

    toggleDialog(state) {
        this.$mdDialog.hide();
        this.widgetsStatus = state;
    }

    toggleWidgetsCheckbox(item) {
        const add = () => this.selected.concat(item);
        const remove = () => _.remove(this.selected, item);
        this.selected = this.exists(item, this.selected) ? remove() : add();
    }

    exists(item, list) {
        return _.includes(list, item);
    }

    select() {
        this.onSelected({
            selected: this.selected
        });
    }
}

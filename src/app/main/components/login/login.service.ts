import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class LoginService {

  static $inject = ['$http'];

  constructor(public $http: ng.IHttpService) { }

  login(username, password) {
    return this.$http.post(`${process.env.API_URL}login`, { 'username': username, 'password': password });
  }
}

import { LOG } from '../constants/log';

export function addLog(log) {
  return {
    type: LOG.ADD_LOG,
    payload: log
  };
}

export function setLogToState(log) {
  return {
    type: LOG.SET_LOG,
    payload: log
  };
}


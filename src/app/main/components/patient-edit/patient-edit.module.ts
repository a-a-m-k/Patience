import * as angular from 'angular';
import {PasswordStrengthDirective } from '../validation-directives/password-strength.directive';
import {EmailValidationDirective } from '../validation-directives/email-validation.directive';
import {PhoneValidationDirective } from '../validation-directives//phone-validation.directive';
import{OnlyDigitValidationDirective} from '../validation-directives/only-digit-validation.directive';
import{OnlyTextValidationDirective} from '../validation-directives/only-text-validation.directive';
import{RequiredValidationDirective} from '../validation-directives/required-validation.directive';


import { PatientEditComponent } from './patient-edit.component';
import * as NgMessageseModule from 'angular-messages';
import { NgModule } from 'angular-ts-decorators';
@NgModule({
  declarations: [PatientEditComponent,RequiredValidationDirective,OnlyTextValidationDirective,PasswordStrengthDirective,EmailValidationDirective,PhoneValidationDirective,OnlyDigitValidationDirective],
  imports: [NgMessageseModule]
})
export class PatientEditModule{}
import { combineReducers } from 'redux';
import { PatientsReducer }  from './patients.reducer';
import { LogReducer } from './log.reducers';
import { UserReducer } from './user.reducers';

export const RootReducer = combineReducers({
    patients: PatientsReducer,
    log: LogReducer,
    user: UserReducer
});

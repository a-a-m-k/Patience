import * as angular from 'angular';
import './add-patient.component.scss';
import { Component, Input, Output, OnInit, OnChanges } from 'angular-ts-decorators';
import { Patient, Abbreviation} from './add-patient.interface';
import { STATES, SEXES } from './add-patient.constant';


@Component({
  selector: 'addPatientComponent',
  template: require('./add-patient.component.html')
})
export class AddPatientComponent implements OnInit, OnChanges {
  @Input() patient: Patient;
  @Output() onCreatePatient;

  static $inject = ['$mdDialog'];
  showHints: boolean = true;
  minDate = new Date();
  maxDate = new Date();
  states: Abbreviation['states'];
  sexes: Abbreviation['sexes'];
    
  constructor(private $mdDialog: any) {};
  
  ngOnInit() {
    this.states = this.getDropDownList(STATES);
    this.sexes = this.getDropDownList(SEXES);
  }
  
  ngOnChanges(changes) {
    if (changes.patient) {
      this.patient = Object.assign({}, this.patient);
    }
  }
    
  onSubmit() {
    this.onCreatePatient({
      $event: {
        patient: this.patient
      }
    });
    this.$mdDialog.hide();
  }
    
  getDropDownList(arr) {
    return arr.reduce((s, v) => s.concat({abbrev: v}), []);
  }
  
  showForm() { 
    this.$mdDialog.show({
      contentElement: '#formDialog',
      parent: angular.element(document.body)
    });
  }

  toggleForm(state) {
    this.$mdDialog.hide();
  }
}

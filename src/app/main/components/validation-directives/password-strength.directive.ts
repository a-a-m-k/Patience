import * as angular from 'angular';
import { IScope, IAugmentedJQuery, IAttributes, INgModelController } from 'angular';
import { Directive } from 'angular-ts-decorators';

@Directive({
  selector: 'password-strength-directive',
  require: 'ngModel',
 restrict:'A',
 link:function(scope: IScope, element: IAugmentedJQuery, attrs: IAttributes, ngModel: INgModelController) {
    ngModel.$parsers.push(function(value) {
        ngModel.$setValidity('empty', /^(\w+\S+)$/.test(value)); 
        ngModel.$setValidity('digit', /[0-9]/.test(value)); 
        ngModel.$setValidity('case', /[A-Z]/.test(value) && /[a-z]/.test(value)); 
        ngModel.$setValidity('special', /[^a-z0-9 ]/i.test(value)); 
        ngModel.$setValidity('length', /^.{8,}$/i.test(value)); 
        return value;
      });
    
  }

})
export class PasswordStrengthDirective {

 
}
export interface EditedDiagnose {
  name: string,
  date: string,
  diagnose: string,
  resolved: string
}
 
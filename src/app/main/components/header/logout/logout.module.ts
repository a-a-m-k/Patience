import { NgModule } from 'angular-ts-decorators';
import { LogoutComponent } from './logout.component';
import ngMaterial = require('angular-material');
import 'angular-material/angular-material.css';


@NgModule({
  id: 'LogoutModule',
  imports: [ngMaterial],
  declarations: [LogoutComponent]
})

export class LogoutModule { }

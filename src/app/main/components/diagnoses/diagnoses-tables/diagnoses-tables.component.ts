import * as angular from 'angular';
import * as _ from 'lodash';
import './diagnoses-tables.component.scss';
import { DiagnosesTablesService } from './diagnoses-tables.service';
import { Component, Input, OnChanges, OnInit, Inject } from 'angular-ts-decorators';
import * as PatientActions from '../../../../actions/patient.actions';

const templateUrl = require('./diagnoses-tables.component.html');

@Component({
    selector: 'diagnoses-tables-component',
    template: templateUrl
})
export class DiagnosesTablesComponent implements OnChanges, OnInit {
  static $inject = ['DiagnosesTablesService', '$ngRedux', '$mdDialog'];
  @Input() patient;
  currentFocused: string;
  diagnoses;  
  rowCol;
  currentFocusedRow;
  currentFocusedCol;
  gridOptions; 
  editedDiagnose;
  properties;
  id;

  constructor(private DiagnosesTablesService: DiagnosesTablesService,
    private $ngRedux) {};
    
    ngOnInit () {
      this.currentFocused = '';
    }
   
    ngOnChanges(changes) {
      if (changes.patient) {
        this.patient = Object.assign({}, this.patient);
      }
      this.gridOptions = this.DiagnosesTablesService.getGridOptions();
      this.gridOptions.appScopeProvider = this;     
      this.gridOptions.data = this.patient.diagnoses;
      this.diagnoses = this.patient.diagnoses;
      
    }


    getFocus(){      
      this.rowCol = this.DiagnosesTablesService.gridApi.cellNav.getFocusedCell();     
      if (this.rowCol) {
          this.currentFocusedRow = this.rowCol.row.entity.id;
          this.currentFocusedCol = this.rowCol.col.colDef.name;
      }  
        
    }

    saveDiagnose({editedDiagnose}) {
      
      this.editedDiagnose = editedDiagnose;      
      this.getFocus();
      this.id = this.currentFocusedRow;      
      this.editedDiagnose.id = this.id;     
      this.diagnoses = _.map(this.diagnoses, 
        item => {
          return (item.id === this.id) ? 
            item = Object.assign({}, this.editedDiagnose) : item;
      });      
      this.patient = {...this.patient, diagnoses: this.diagnoses};
      console.log(this.patient);      
      this.$ngRedux.dispatch(PatientActions.updateDiagnoses(this.patient));
    }
 }



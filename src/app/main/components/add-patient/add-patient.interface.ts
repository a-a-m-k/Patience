export interface Patient {
  id: number,
  firstName: string,
  lastName: string,
  sex: string,
  birthData: string,
  deathData: string,
  phone: string,
  workPhone: string,
  email: string,
  house: string,
  state: string,
  street: string,
  city: string,
  zip: string,
  business: string,
  submissionDate: string,
  comments: string
}

export interface Abbreviation {
  states: string[],
  sexes: string[]
}
import { RootReducer } from './reducers';
import { createStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';

appRoutes.$inject = ['$locationProvider'];

export function appRoutes($locationProvider: angular.ILocationProvider) {
  $locationProvider.html5Mode({
    enabled: false,
    requireBase: false
  });
}

ngReduxConfig.$inject = ['$ngReduxProvider'];

export function ngReduxConfig($ngReduxProvider) {  
  const store = createStore(RootReducer, devToolsEnhancer({}));
  $ngReduxProvider.provideStore(store);
}

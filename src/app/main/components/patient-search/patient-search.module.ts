import { Inject } from 'angular-ts-decorators';
import { NgModule } from 'angular-ts-decorators';
import { SearchComponent } from './patient-search.component';
import { SearchService } from './patient-search.service';

@NgModule({
  id: 'searchModule',
  declarations: [SearchComponent],
  providers: [
    { provide: 'SearchService', useClass: SearchService },
  ]
})

export class searchModule {
}

import * as angular from 'angular';
import { Main } from './main.component';
import { mainRoutes } from './main.config';
import { componentsModule } from './components/components.module';
import { containersModule } from './containers/containers.module';
import { MenuModule } from './common/menu/menu.module';
import { LoginModule } from './components/login/login.module';

export const mainModule = angular
  .module('app.main', [
    componentsModule,
    containersModule,
    MenuModule.name
   // LoginModule.name
  ])
  .component('main', Main)
  .config(mainRoutes)
  .name;
